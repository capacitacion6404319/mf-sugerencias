import { defineComponent } from "vue";

import CmpSugerencias from "../../components/business/cmp-sugerencias/cmp-sugerencias.vue";

export default defineComponent({
    components: {
        CmpSugerencias
    }
})