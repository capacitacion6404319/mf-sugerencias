import CmpSugerencia from "./cmp-sugerencia.vue";
import { PLANTILLA_HTML } from "../../../../constants/storybook.constant";

const TITULO = `CMP-SUGERENCIA`;

const SUMMARY = `
    El componente <strong>${TITULO}</strong> representa una sugerencia de video para el usuario, en el se especifícan las siguientes <strong>propiedades</strong>:`;

const PROPS = `
    <li><p class="erp-storybook-texto"><strong>item</strong>: Objeto que contiene la información de una sugerencia de video</p></li>
`;

const COMPONENT = `
<div class="component-main" style="margin: 20px 0px;">
    <cmp-sugerencia v-bind="args"/>
</div>
`

const COMPONENTS = `

`;

const AUTOR = `Luis Amat`;

const HISTORY = `
    <span>[2024-05-10][Luis Amat] Creación del componente</span>
`;

export default {
    title: 'components/business/cmp-sugerencias/cmp-sugerencia',
    component: CmpSugerencia,
    //tags: ['autodocs']
}

const Template = args => ({
    components: { CmpSugerencia },
    setup() {
        return { args }
    },    
    template: PLANTILLA_HTML.replace("$$TITULO$$", `${TITULO}`).replace("$$SUMMARY$$", `${SUMMARY}`).replace("$$PROPS$$", `${PROPS}`).replace("$$COMPONENT$$", `${COMPONENT}`).replace("$$COMPONENTS$$", `${COMPONENTS}`).replace("$$AUTOR$$", `${AUTOR}`).replace("$$HISTORY$$", `${HISTORY}`)
})

export const CmpSugerenciaBase = Template.bind({});
CmpSugerenciaBase.storyName = "cmp-sugerencia-base";
CmpSugerenciaBase.args = {
    "id": 1,
    "title": "Michael Jackson – You Rock My World [Lyrics/Sub español]",
    "thumbnail": "https://i.ytimg.com/vi/XqoTEpyZisc/hqdefault.jpg?sqp=-oaymwEwCKgBEF5IWvKriqkDIwgBFQAAiEIYAfABAfgB_gmAAtAFigIMCAAQARgsIE8ofzAP&rs=AOn4CLB1ytb9eNYsR4GoUbiUXy6JLmYdvg",
    "video": "https://luminous-crostata-ee8f80.netlify.app/src/assets/erp-mp4/my-world-my-life.mp4",
    "author": "Michael Jackson",
    "date": "5 years ago"
};