export const SUGERENCIAS_STORE_GET_SUGERENCIAS = "SugerenciasStore/getSugerencias";

export const SUGERENCIAS_STORE_LISTAR_SUGERENCIAS = "SugerenciasStore/listarSugerencias";
export const SUGERENCIAS_STORE_REINICIAR = "SugerenciasStore/reiniciar";

import { listarSugerencias } from "../../core/sugerencias/infraestructure/adapters/in/sugerencias.service";

export default {
    namespaced: true,
    state: {
        sugerencias: [],
    },
    mutations: {
        LISTAR_SUGERENCIAS(state: any, sugerencias: []) {
            state.sugerencias = sugerencias;
        },
        REINICIAR(state: any) {
            state.sugerencias = [];
        }
    },
    actions: {
        listarSugerencias({ commit }: any) {
            const sugerencias = listarSugerencias();
            commit('LISTAR_SUGERENCIAS', sugerencias);
        },
        reiniciar({ commit }: any) {
            commit('REINICIAR');
        }
    },
    getters: {
        getSugerencias(state: any) {
            return state.sugerencias;
        }
    }
}