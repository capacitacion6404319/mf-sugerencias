import { SingletonBase } from "../../../shared/domain/singleton-base.domain";
import { ListarSugerenciasPort } from "../../infraestructure/ports/in/listar-sugerencias.port";
import { Sugerencia } from "../../domain/sugerencia.domain";

import sugerencias from "../../../../assets/resources/data-dummy/sugerencias.json";

export class ListarSugerenciasUseCase extends SingletonBase implements ListarSugerenciasPort {

    constructor() {
        super();
    }

    listarSugerencias(): Sugerencia[] {
        return sugerencias;
    }
}