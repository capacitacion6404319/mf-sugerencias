import { createStore } from 'vuex'

import SugerenciasStore from "./business/cmp-sugerencias.store"

export default createStore({
  modules: {
    SugerenciasStore
  }
})
