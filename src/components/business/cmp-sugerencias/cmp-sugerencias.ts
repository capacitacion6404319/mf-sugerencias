import { defineComponent, onMounted, onUnmounted } from "vue";

import CmpSugerencia from "./cmp-sugerencia/cmp-sugerencia.vue";

import useSugerencias from "../../../composables/business/cmp-sugerencias.composable";

export default defineComponent({
    components: {
        CmpSugerencia
    },
    setup() {
        const PREFIX = "cmp-sugerencias";

        const { sugerencias, listarSugerencias, reiniciar } = useSugerencias();

        onMounted(() => {
            listarSugerencias();
        })

        onUnmounted(() => {
            reiniciar
        })

        return {
            PREFIX,
            sugerencias
        }
    }
})
