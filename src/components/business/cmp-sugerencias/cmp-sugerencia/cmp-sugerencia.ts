import { defineComponent } from "vue";

export default defineComponent({
    props: {
        item: {
            type: Object,
            default: null
        }
    },
    setup(props) {
        const PREFIX = "cmp-sugerencia";

        const seleccionarVideo = () => {
            window.dispatchEvent(
                new CustomEvent('evt-cmp-sugerencia-seleccionar-video', {
                    detail: {
                        item: props.item,
                    },
                })
            );
        }

        return {
            PREFIX,
            seleccionarVideo
        }
    }
})
