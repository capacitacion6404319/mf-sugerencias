import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router'

import VwSugerencias from '../views/vw-sugerencias/vw-sugerencias.vue'

const routes: Array<RouteRecordRaw> = [
  {
    path: "/",    
    component: VwSugerencias
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
