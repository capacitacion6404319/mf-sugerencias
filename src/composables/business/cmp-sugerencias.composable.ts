import { computed } from "vue";
import { useStore } from "vuex";

import {
    SUGERENCIAS_STORE_GET_SUGERENCIAS,
    SUGERENCIAS_STORE_LISTAR_SUGERENCIAS,
    SUGERENCIAS_STORE_REINICIAR
} from "../../store/business/cmp-sugerencias.store";

import { executeAction } from "../../utils/vuex.util";

export default function useSugerencias() {
    const store = useStore();

    const listarSugerencias = () => {
        executeAction(SUGERENCIAS_STORE_LISTAR_SUGERENCIAS, null);
    }

    const reiniciar = () => {
        executeAction(SUGERENCIAS_STORE_REINICIAR, null);
    }

    const sugerencias = computed(
        () => store.getters[SUGERENCIAS_STORE_GET_SUGERENCIAS]
    );

    return {
        sugerencias,
        listarSugerencias,
        reiniciar
    }
}