# Fundamentos de Microfrontends: Ejercicio Práctico

## Microfrontend Sugerencias ##

### Enunciado ###

Desarrollar una lista de videos sugeridos al usuario, este debe recibir disparar un evento que será recibido por el microfrontend de video y este reproducira el video seleccionado por el usuario.
[Prototipo Figma: Ejercicio Práctico 2](https://www.figma.com/design/x9uqJOn72FkfWW0xMFqBvk/Pruebas?node-id=66-47&t=ROcJPDHLFIyKWPZU-0)

### Objetivos ###
- Comprender los fundamentos de microfrotends y como se comunican entre sí.
- Desarrollo orientado a componentes basado en figma.
- Análisis e implementación de patrón vuex.
- Análisis e implementación de clean architecture.

### Consideraciones ###
- No es necesario implementar un backend para obtener los datos, puede usar data dummy o de internet.
- Tener instalado NodeJs v16.20.2