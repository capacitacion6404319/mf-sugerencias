import { ListarSugerenciasPort } from "../../ports/in/listar-sugerencias.port";

import { ListarSugerenciasUseCase } from "../../../application/use-case/listar-sugerencias.use-case";

export const listarSugerencias = () => {
    const listarSugerenciasPort: ListarSugerenciasPort = ListarSugerenciasUseCase.getInstance(ListarSugerenciasUseCase);
    return listarSugerenciasPort.listarSugerencias();
}