import { Sugerencia } from "../../../domain/sugerencia.domain";

export interface ListarSugerenciasPort {
    listarSugerencias(): Sugerencia[];
}